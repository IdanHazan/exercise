import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

@Entity()
export class Tweet {

    @PrimaryGeneratedColumn()
    tweetId: string;

    @Column()
    content: string;

    @Column()
    username: string;

    @Column()
    timestamp: string;
}
@Entity()
export class Like {
    @PrimaryGeneratedColumn()
    likeId: string;

    @Column()
    tweetId: string;

    @Column()
    username: string;

    @Column()
    timestamp: string;
}
@Entity()
export class ReTweet {

    @PrimaryGeneratedColumn()
    reTweetId: string;

    @Column()
    tweetId: string;

    @Column()
    username: string;

    @Column()
    timestamp: string;
}
