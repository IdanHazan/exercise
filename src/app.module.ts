import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppService } from './app.service';
import { Like, ReTweet, Tweet } from './entities';
import { RetweetsController } from './retweets/retweets.controller';
import { RetweetsService } from './retweets/retweets.service';

const dbName = process.env.dbName || 'test';
const dbPort = +process.env.dbPort || 5432;
const dbHost = process.env.dbHost || 'postgres-container';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: "postgres",
      host: dbHost,
      port: dbPort,
      username: 'postgres',
      password: 'postgres',
      database: dbName,
      entities: [Like, ReTweet, Tweet],
      autoLoadEntities: true,
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Like]),
    TypeOrmModule.forFeature([Tweet]),
    TypeOrmModule.forFeature([ReTweet]),
  ],
  controllers: [AppController, RetweetsController],
  providers: [AppService, RetweetsService],
})
export class AppModule { }

