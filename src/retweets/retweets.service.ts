import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';
import { Tweet, ReTweet } from '../entities';

@Injectable()
export class RetweetsService {
    constructor(@InjectRepository(Tweet) private readonly _tweetsRepository: Repository<Tweet>,
        @InjectRepository(ReTweet) private readonly _reTweetsRepository: Repository<ReTweet>) { }

    public async getAllRetweets() {
        let returnRetweetsArray = new Array();
        let retweets = await this._reTweetsRepository.find({});
        for (let retweet of retweets) {
            let tweet = await this._tweetsRepository.findOne({ tweetId: retweet.tweetId });
            returnRetweetsArray.push(await this.addPropertiesToRetweet(retweet, tweet));
        }
        return returnRetweetsArray;

    }
    private async addPropertiesToRetweet(reTweet, tweet) {
        let returnObj = {
            content: tweet.content,
            retweet_user: reTweet.username,
            tweet_id: tweet.tweetId,
            tweet_user: tweet.username,
            timestamp: reTweet.timestamp
        }
        return returnObj;
    }
}
