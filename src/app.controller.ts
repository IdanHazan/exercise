import { Controller, Get, Post, Body, Param, Req, InternalServerErrorException } from '@nestjs/common';
import { AppService } from './app.service';
import { Tweet } from './entities';


@Controller('/tweets')
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get()
  private async getTweets() {
    try {
      return await this.appService.getAllTweets();
    }
    catch (error) {
      throw new InternalServerErrorException(`failed to get all tweets, error: ${error} `);
    }
  }
  @Post()
  private async addTweet(@Body() tweet: Tweet) {
    try {
      return await this.appService.addTweet(tweet);
    } catch (error) {
      throw new InternalServerErrorException(`failed to add tweet, error: ${error} `);
    }
  }
  @Post(':tweetId/likes')
  private async createLikeToPost(@Param('tweetId') tweetId: String, @Req() req) {
    try {
      return await this.appService.makeLikeToTweet(tweetId, req.body.username);
    }
    catch (error) {
      throw new InternalServerErrorException(`failed to like tweet id:${tweetId}, error: ${error} `);
    }
  }

  @Post(':tweetId/retweet')
  private async reTweetPost(@Param('tweetId') tweetId: String, @Req() req) {
    try {
      return await this.appService.reTweetPost(tweetId, req.body.username);
    } catch (error) {
      throw new InternalServerErrorException(`failed to retweet tweet id:${tweetId}, error: ${error} `);
    }
  }
}
