FROM node:latest

ENV dbName='test'

ENV dbPort=5432

ENV dbHost='postgres-container'

WORKDIR /app

COPY package.json /app

RUN npm install

COPY . .

EXPOSE 3000

CMD [ "npm","run","start"]
