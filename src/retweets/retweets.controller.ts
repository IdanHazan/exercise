
import { RetweetsService } from './retweets.service';
import { Controller, Get, Post, Body, Param, Req, InternalServerErrorException } from '@nestjs/common';


@Controller('retweets')
export class RetweetsController {
    constructor(private readonly retweetService: RetweetsService) { }

    @Get()
    private async getRetweets() {
        try {
            return await this.retweetService.getAllRetweets();
        } catch (error) {
            throw new InternalServerErrorException(error);
        }
    }

}
