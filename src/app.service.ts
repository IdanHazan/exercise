import { Injectable, NotFoundException, InternalServerErrorException } from '@nestjs/common';
import { Like, Tweet, ReTweet } from './entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AppService {

  constructor(@InjectRepository(Like) private readonly _likesRepository: Repository<Like>,
    @InjectRepository(Tweet) private readonly _tweetsRepository: Repository<Tweet>,
    @InjectRepository(ReTweet) private readonly _reTweetsRepository: Repository<ReTweet>) { }

  public async getAllTweets() {
    let returnArrayOfTweets = new Array();
    const tweetsInDb = await this._tweetsRepository.find({});
    for (let tweet of tweetsInDb) {
      returnArrayOfTweets.push(await this.addPropertiesToTweet(tweet));
    }

    return returnArrayOfTweets;
  }
  public async addPropertiesToTweet(tweet) {
    let likesInTweet = (await this._likesRepository.find({ tweetId: tweet.tweetId })).length;
    let retweetInTweets = (await this._reTweetsRepository.find({ tweetId: tweet.tweetId })).length;
    let tweetsReturnObj = JSON.parse(JSON.stringify(tweet));
    tweetsReturnObj.likesCount = likesInTweet;
    tweetsReturnObj.retweetsCount = retweetInTweets;
    return tweetsReturnObj;

  }

  public async addTweet(tweet: Tweet) {
    let dateTime: Date = new Date();
    let tweetToAdd = new Tweet();
    tweetToAdd.content = tweet.content;
    tweetToAdd.username = tweet.username;
    tweetToAdd.timestamp = dateTime.toISOString().slice(0, 10);
    return this._tweetsRepository.save(tweetToAdd);
  }

  public async makeLikeToTweet(id, username) {
    if (await this.checkTweetExistsById(id)) {
      let dateTime: Date = new Date();
      let likeToAdd = new Like();
      likeToAdd.tweetId = id;
      likeToAdd.username = username;
      likeToAdd.timestamp = dateTime.toISOString().slice(0, 10)
      return this._likesRepository.save(likeToAdd);
    }
    return new InternalServerErrorException(`tweet not found`);
  }
  public async reTweetPost(id, username) {
    if (await this.checkTweetExistsById(id)) {
      let dateTime: Date = new Date();
      let reTweetToAdd = new ReTweet()
      reTweetToAdd.tweetId = id;
      reTweetToAdd.username = username;
      reTweetToAdd.timestamp = dateTime.toISOString().slice(0, 10);
      return this._reTweetsRepository.save(reTweetToAdd);
    }
    return new InternalServerErrorException(`tweet not found`);
  }

  private async checkTweetExistsById(tweetId): Promise<boolean> {
    if ((await this._tweetsRepository.find({ tweetId: tweetId })).length !== 0) {
      return true;
    }
    return false;

  }
}
